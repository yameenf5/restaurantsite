import { useQuery } from "@tanstack/react-query";
import { PropsWithChildren } from "react";
import { getSession } from "../utils/authSession";

type IslogedInProps = {
  fallback: React.ReactElement,
}

export function IslogedIn({ fallback, children }: PropsWithChildren<IslogedInProps>) {
  const { data: session } = useQuery(['getSession'], getSession);
  if (session === null ) {
    return <>{fallback}</>
  }
  if (session !== null ) {
    return <>{children}</>
  }
}
