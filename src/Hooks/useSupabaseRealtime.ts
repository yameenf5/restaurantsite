import { RealtimePostgresChangesPayload } from "@supabase/supabase-js"
import { useEffect } from "react"
import { supabaseClient } from "../utils/supabaseClient";


type Payload = RealtimePostgresChangesPayload<{ [key: string]: any; }>


export const useSupabaseSubscribe = ({ channelName, tableName, onCreate, onUpdate, onDelete }: {
  channelName: string,
  tableName: string,
  onCreate?: (payload: Payload) => void,
  onUpdate?: (payload: Payload) => void,
  onDelete?: (payload: Payload) => void
}
) => {
  useEffect(() => {
    const subscription = supabaseClient.channel(channelName)
      .on(
        'postgres_changes',
        { event: '*', schema: 'public', table: tableName},
        (payload) => {
          switch (payload.eventType) {
            case 'INSERT':
              if (onCreate !== undefined)
                onCreate(payload)
              break
            case 'DELETE':
              if (onDelete !== undefined)
                onDelete(payload)
              break
            case 'UPDATE':
              if (onUpdate !== undefined) {
                onUpdate(payload)
              }
              break
          }
        }
      )
      .subscribe()

    return () => {
      subscription.unsubscribe()
    }
  }, [])
}
