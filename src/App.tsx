import { QueryClient, QueryClientProvider } from "@tanstack/react-query"
import { BrowserRouter, Route, Routes } from "react-router-dom"
import { LandingPage } from "./pages/LandingPage"
import { Toaster } from "react-hot-toast"
import { IslogedIn } from "./Routes/IslogedIn"
import { SignUp } from "./pages/SignUp"
import { SignIn } from "./pages/SignIn"
import { Menu } from "./pages/Menu"
import { Admin } from "./pages/Admin"
import { AddProduct } from "./pages/AddProduct"
import { HanldeProducts } from "./pages/HandleProduct"
import { IsAdmin } from "./Routes/IsAdmin"
import { CreateOrder } from "./pages/CreateOrder"
import { InShopOrders } from "./pages/InShopOrder"
import { UserAddressDetails } from "./pages/UserAddressDetails"
import { OrderHistory } from "./pages/OrderHistory"
import { PrivateRoute } from "./Routes/PrivateRoute"
import { UncompletedOrders } from "./pages/UncompletedOrders"
import { OnlineOrders } from "./pages/OnlineOrders"
import { ErrorPage } from "./components/Error"
import { AuthorizedPage } from "./components/AuthorizedPage"

export default function AppWraper() {
  const queryClient = new QueryClient()

  return (
    <>
      <QueryClientProvider client={queryClient}>
        <BrowserRouter>
          <Toaster />
          <App />
        </BrowserRouter>
      </QueryClientProvider>
    </>
  )
}

function App() {
  return (
    <>
      <Routes>
        <Route path="/" element={<LandingPage />} />
        <Route path="/menu" element={<Menu />} />
        <Route path="/signUp" element={<IslogedIn fallback={<SignUp />}><AuthorizedPage/></IslogedIn>} />
        <Route path="/signIn" element={<IslogedIn fallback={<SignIn />}><AuthorizedPage/></IslogedIn>} />
        <Route path="/addressdetails" element={<PrivateRoute fallback={<ErrorPage />}> <UserAddressDetails /> </PrivateRoute>} />
        <Route path="/orderhistory" element={<PrivateRoute fallback={<ErrorPage />}> <OrderHistory /> </PrivateRoute>} />
        <Route path="/admin" element={<IsAdmin fallback={<ErrorPage />}><Admin /></IsAdmin>} />
        <Route path="/admin/additem" element={<IsAdmin fallback={<ErrorPage />}><AddProduct /></IsAdmin>} />
        <Route path="/admin/handleitem" element={<IsAdmin fallback={<ErrorPage />}><HanldeProducts /></IsAdmin>} />
        <Route path="/admin/createorder" element={<IsAdmin fallback={<ErrorPage />}><CreateOrder /></IsAdmin>} />
        <Route path="/admin/inshopOrder" element={<IsAdmin fallback={<ErrorPage />}><InShopOrders /></IsAdmin>} />
        <Route path="/admin/handleOnlineOrders" element={<IsAdmin fallback={<ErrorPage />}><UncompletedOrders /></IsAdmin>} />
        <Route path="/admin/onlineOrders" element={<IsAdmin fallback={<ErrorPage />}><OnlineOrders /></IsAdmin>} />
      </Routes>
    </>
  )
}
