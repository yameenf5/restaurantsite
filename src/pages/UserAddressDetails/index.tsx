import { Link } from "react-router-dom";
import Navbar from "../../components/Navbar";
import { SubmitHandler, useForm } from "react-hook-form";
import { typeAddressDetailsEdit, typeAddressDetailsInsert, typeAddressDetailsView } from "../../../types";
import { AiOutlineWarning } from "react-icons/ai"
import { useMutation, useQuery, useQueryClient } from "@tanstack/react-query";
import { addAddressDetails, deleteAddressDetails, editAddressDetails, getAddressDetaials } from "./handledUserDetails";
import { toast } from "react-hot-toast";
import { getSession } from "../../utils/authSession";
import { useState } from "react";

export function UserAddressDetails() {
  const { data: session } = useQuery(['getSession'], getSession)
  const userId = session?.user.id
  const { data: addressDetails } = useQuery(['getAddresses', userId], () => getAddressDetaials({ customerId: userId }))

  return <>
    <Navbar />
    <section className="bg-white h-screen">
      <div className="lg:min-h-screen lg:grid-cols-12">
        <section
          className="relative flex h-32 items-end bg-gray-900 lg:col-span-5 lg:h-full xl:col-span-6"
        >
          <img
            alt="Night"
            src="https://images.unsplash.com/photo-1567620905732-2d1ec7ab7445?ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D&auto=format&fit=crop&w=780&q=80"
            className="absolute inset-0 h-full w-full object-cover opacity-80"
          />

          <div className="hidden lg:relative lg:block lg:p-12">
            <Link className="block text-white" to={"/"}>
              <span className="sr-only">Home</span>
            </Link>

            <h2 className="mt-6 text-2xl font-bold text-white sm:text-3xl md:text-4xl">
              Welcome to JUST EAT {/*TODO*/}
            </h2>

            <p className="mt-4 leading-relaxed text-white">
              We provide the best and fresh food.
            </p>
          </div>
        </section>
        <div className="mx-auto">
          <div className="card rounded-none bg-black text-neutral-content m-1 lg:m-10">
            <div className="card-body ">
              <h2 className="card-title uppercase lg:text-xl text-sm">Previously Added Address ...</h2>
              <div>
                <ul className="flex flex-col" >
                  {addressDetails && addressDetails.map(
                    addressDetail => (
                      <li className="list-disc m-1" key={addressDetail.id}>
                        <ViewEditAddressModal addressDetails={addressDetail} />
                      </li>
                    ))}
                </ul>
              </div>
              <div className="card-actions justify-end">
                <label htmlFor="addressForm" className="btn btn-ghost rounded-none border border-white">Add</label>
                <AddressModal />
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
  </>
}


function AddressModal() {
  const queryClient = useQueryClient()
  const { mutate: addAddressDetailsFn, isLoading: isAddingAddress } = useMutation(['addAddress'], addAddressDetails,
    {
      onSuccess: () => {
        queryClient.invalidateQueries(['getAddresses']);
        toast.success('Address Added Successfully');
        reset()
      },
      onError: () => {
        toast.error('Error in Adding Address');
      }
    }
  )
  const { register, handleSubmit, formState: { errors }, reset } = useForm<typeAddressDetailsInsert>({
  });

  const onSubmit: SubmitHandler<typeAddressDetailsInsert> = (fdata) => {
    addAddressDetailsFn(fdata)
  };
  return <>
    <input type="checkbox" id="addressForm" className="modal-toggle" />
    <div className="modal">
      <div className="modal-box rounded-none text-black">
        <h3 className="font-bold text-lg text-center">Add Your Address</h3>
        <form onSubmit={handleSubmit(onSubmit)} className="grid grid-cols-3">
          <div className="col-span-3">
            <label htmlFor="addressName" className="block text-sm font-medium text-gray-700">
              Address Title
            </label>
            {errors.addressName && <span className="text-yellow-300 flex"><AiOutlineWarning className="m-1" /> {errors.addressName?.message}</span>}
            <input
              type="text"
              id="CustomerName"
              className="mt-1 w-full rounded-md border-gray-200 bg-white text-sm text-gray-700 shadow-sm"
              {...register("addressName", { required: "Title is required" })}
            />
          </div>
          <div className="col-span-3 ">
            <label
              htmlFor="city"
              className="block text-sm font-medium text-gray-700"
            >
              City
            </label>
            {errors.city && <span className="text-yellow-300 flex"><AiOutlineWarning className="m-1" /> {errors.city?.message}</span>}
            <input
              type="text"
              id="area"
              className="mt-1 w-full rounded-md border-gray-200 bg-white text-sm text-gray-700 shadow-sm"
              {...register("city", { required: "City is required" })}
            />
          </div>
          <div className="col-span-3 ">
            <label
              htmlFor="area"
              className="block text-sm font-medium text-gray-700"
            >
              Area with Lane
            </label>
            {errors.area && <span className="text-yellow-300 flex"><AiOutlineWarning className="m-1" /> {errors.area?.message}</span>}
            <input
              type="text"
              id="area"
              className="mt-1 w-full rounded-md border-gray-200 bg-white text-sm text-gray-700 shadow-sm"
              {...register("area", { required: "Area is required" })}
            />
          </div>
          <div className="col-span-3">
            <label
              htmlFor="landmark"
              className="block text-sm font-medium text-gray-700"
            >
              Landmark
            </label>
            {errors.landmark && <span className="text-yellow-300 flex"><AiOutlineWarning className="m-1" /> {errors.landmark?.message}</span>}
            <input
              type="text"
              id="landmark"
              className="mt-1 w-full rounded-md border-gray-200 bg-white text-sm text-gray-700 shadow-sm"
              {...register("landmark", { required: "Landmark is required" })}
            />
          </div>
          <div className="col-span-3">
            <label
              htmlFor="pincode"
              className="block text-sm font-medium text-gray-700"
            >
              Pincode
            </label>
            {errors.pincode && <span className="text-yellow-300 flex"><AiOutlineWarning className="m-1" /> {errors.pincode?.message}</span>}
            <input
              type="number"
              id="pincode"
              className="mt-1 w-full rounded-md border-gray-200 bg-white text-sm text-gray-700 shadow-sm"
              {...register("pincode", { required: "Pincode is required" })}
            />
          </div>

          <div className="col-span-3">
            <label
              htmlFor="houseNo"
              className="block text-sm font-medium text-gray-700"
            >
              House No
            </label>
            {errors.houseNo && <span className="text-yellow-300 flex"><AiOutlineWarning className="m-1" /> {errors.houseNo?.message}</span>}
            <input
              type="number"
              id="houseNo"
              className="mt-1 w-full rounded-md border-gray-200 bg-white text-sm text-gray-700 shadow-sm"
              {...register("houseNo", { required: "House No is required" })}
            />
          </div>

          <button className={`${isAddingAddress ? "loading loading-spinner" : ""}  btn bg-black mt-3 hover:bg-gray-700 text-white font-bold focus:outline-black focus:ring active-bg-gray-400 rounded-none`}>
            Add
          </button>
        </form>
        <div className="modal-action">
          <label htmlFor="addressForm" className="btn rounded-none">Close!</label>
        </div>
      </div>
    </div>
  </>
}

function ViewEditAddressModal({ addressDetails }: { addressDetails: typeAddressDetailsView }) {
  const queryClient = useQueryClient()
  const [view, setView] = useState(true);
  const { mutate: editAddressDetailsFn, isLoading: isAddingAddress } = useMutation(['addAddress'], editAddressDetails,
    {
      onSuccess: () => {
        queryClient.invalidateQueries();
        toast.success('Address Edited Successfully');
        setView(true);
      },
      onError: () => {
        toast.error('Error in Editing Address');
      }
    }
  )
  const { mutate: deleteAddressDetailFn, isLoading: deleting } = useMutation(['deleteAddress'], deleteAddressDetails,
    {
      onSuccess: () => {
        toast.success('Address Deleted Successfully');
        queryClient.invalidateQueries(['getAddresses']);
      },
      onError: () => {
        toast.error('Error in Deleting Address');
      }
    }
  )
  const { register, handleSubmit, formState: { errors } } = useForm<typeAddressDetailsEdit>({
    defaultValues: {
      addressName: addressDetails.addressName,
      city: addressDetails.city,
      area: addressDetails.area,
      landmark: addressDetails.landmark,
      pincode: addressDetails.pincode,
      houseNo: addressDetails.houseNo,
    }
  });

  const onSubmit: SubmitHandler<typeAddressDetailsEdit> = (fdata) => {
    editAddressDetailsFn({ id: addressDetails.id, updatedDetails: fdata })
  };

  return <>
    <label htmlFor={addressDetails.id} className="btn bg-black text-white rounde-none border-none btn-outline">{addressDetails.addressName}</label>
    <input type="checkbox" id={addressDetails.id} className="modal-toggle" />
    <div className="modal">
      <div className="modal-box rounded-none text-black">
        <h3 className="font-bold text-lg text-center">Add Your Address</h3>
        <form onSubmit={handleSubmit(onSubmit)} className="grid grid-cols-3">
          <div className="col-span-3">
            <label htmlFor="addressName" className="block text-sm font-medium text-gray-700">
              Address Title
            </label>
            {errors.addressName && <span className="text-yellow-300 flex"><AiOutlineWarning className="m-1" /> {errors.addressName?.message}</span>}
            <input
              type="text"
              id="CustomerName"
              className="mt-1 w-full rounded-md border-gray-200 bg-white text-sm text-gray-700 shadow-sm "
              {...register("addressName", { required: "Title is required" })}
              readOnly={view}
            />
          </div>
          <div className="col-span-3 ">
            <label
              htmlFor="city"
              className="block text-sm font-medium text-gray-700"
            >
              City
            </label>
            {errors.city && <span className="text-yellow-300 flex"><AiOutlineWarning className="m-1" /> {errors.city?.message}</span>}
            <input
              type="text"
              id="area"
              className="mt-1 w-full rounded-md border-gray-200 bg-white text-sm text-gray-700 shadow-sm"
              {...register("city", { required: "City is required" })}
              readOnly={view}
            />
          </div>
          <div className="col-span-3 ">
            <label
              htmlFor="area"
              className="block text-sm font-medium text-gray-700"
            >
              Area with Lane
            </label>
            {errors.area && <span className="text-yellow-300 flex"><AiOutlineWarning className="m-1" /> {errors.area?.message}</span>}
            <input
              type="text"
              id="area"
              className="mt-1 w-full rounded-md border-gray-200 bg-white text-sm text-gray-700 shadow-sm"
              {...register("area", { required: "Area is required" })}
              readOnly={view}
            />
          </div>
          <div className="col-span-3">
            <label
              htmlFor="landmark"
              className="block text-sm font-medium text-gray-700"
            >
              Landmark
            </label>
            {errors.landmark && <span className="text-yellow-300 flex"><AiOutlineWarning className="m-1" /> {errors.landmark?.message}</span>}
            <input
              type="text"
              id="landmark"
              className="mt-1 w-full rounded-md border-gray-200 bg-white text-sm text-gray-700 shadow-sm"
              {...register("landmark", { required: "Landmark is required" })}
              readOnly={view}
            />
          </div>
          <div className="col-span-3">
            <label
              htmlFor="pincode"
              className="block text-sm font-medium text-gray-700"
            >
              Pincode
            </label>
            {errors.pincode && <span className="text-yellow-300 flex"><AiOutlineWarning className="m-1" /> {errors.pincode?.message}</span>}
            <input
              type="number"
              id="pincode"
              className="mt-1 w-full rounded-md border-gray-200 bg-white text-sm text-gray-700 shadow-sm"
              {...register("pincode", { required: "Pincode is required" })}
              readOnly={view}
            />
          </div>

          <div className="col-span-3">
            <label
              htmlFor="houseNo"
              className="block text-sm font-medium text-gray-700"
            >
              House No
            </label>
            {errors.houseNo && <span className="text-yellow-300 flex"><AiOutlineWarning className="m-1" /> {errors.houseNo?.message}</span>}
            <input
              type="text"
              id="houseNo"
              className="mt-1 w-full rounded-md border-gray-200 bg-white text-sm text-gray-700 shadow-sm"
              {...register("houseNo", { required: "House No is required" })}
              readOnly={view}
            />
          </div>
          {
            !view && <button
              className={`${isAddingAddress ? "loading loading-spinner" : ""}  
                     btn bg-black mt-3 hover:bg-gray-700 text-white font-bold focus:outline-black focus:ring active-bg-gray-400 rounded-none`}>
              Update</button>
          }
        </form>
        {view && <button className="btn bg-black mt-3 hover:bg-gray-700 text-white font-bold focus:outline-black focus:ring active-bg-gray-400 rounded-none"
          onClick={() => { setView(false) }}>
          Edit
        </button>}
        <div className="modal-action">
          <button className={`${deleting? "loading": ""} btn bg-black hover:bg-gray-700 text-white font-bold focus:outline-black focus:ring active-bg-gray-400 rounded-none1`}
            onClick={() => { deleteAddressDetailFn({ id: addressDetails.id }) }}>
            Delete
          </button>
          <label htmlFor={addressDetails.id} className="btn rounded-none">Close!</label>
        </div>
      </div>
    </div>
  </>

}
