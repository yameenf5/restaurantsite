import { useState } from "react";
import Navbar from "../../components/Navbar";
import { useMutation, useQuery } from "@tanstack/react-query";
import { getItemPicture, getMenuItem, orderItem } from "./handleMenu";
import { typeViewProductData } from "../../../types";
import { GrFormNext, GrFormPrevious } from "react-icons/gr";
import { getCategories } from "../HandleProduct/handleProduct";
import { BiRupee } from "react-icons/bi";
import { getSession } from "../../utils/authSession";
import { getAddressDetaials } from "../UserAddressDetails/handledUserDetails";
import { Link } from "react-router-dom";
import { toast } from "react-hot-toast";

export function Menu() {
  const [index, setIndex] = useState(0);
  const [category, setCategory] = useState('All');
  const [item, setItem] = useState('');

  const { data: categories } = useQuery(['getCategories'], getCategories)
  const { data, isLoading, isPreviousData } = useQuery(['getMenuItems', { index, category: category, itemName: item }],
    () => getMenuItem({ indexValue: index, category: category, itemName: item }),
    { keepPreviousData: true })

  return (<>
    <Navbar />
    <div className="m-2 lg:border lg:border-black lg:m-10 ">
      <section>
        <div className="fixed lg:relative z-10 lg:z-20  w-full mt-10 lg:m-0 bg-gray-100 lg:bg-white -ml-2">
          <header className="mt-5 lg:mt-10 mb-0 p-0 sm:py-5">
            <div className="mx-auto max-w-screen-xl px-4 sm:px-6 lg:px-8">
              <div className="flex h-4 items-center justify-between">
                <nav aria-label="Global" className="mx-auto" >
                  <ul className="flex gap-6 text-lg" >
                    {
                      categories &&
                      categories.map((category) => (<button key={category.id} onClick={() => setCategory(category.categoryName)} className="uppercase text-gray-500 transition hover:text-gray-500/75 btn-ghost rounded-none lg:p-4 lg:text-xl text-xs">
                        {category.categoryName}</button>))
                    }
                  </ul>
                </nav>
              </div>
            </div>
          </header >
          <div className="relative mx-10 mt-0  lg:mt-2 lg:w-1/5 sm:w-1/2 p-2">
            <label htmlFor="Search" className="sr-only"> Search </label>
            <input
              type="text"
              id="Search"
              placeholder="Search for..."
              className="w-full rounded-md border-gray-200 py-2.5 pe-10 shadow-sm sm:text-sm"
              value={item}
              onChange={(e) => setItem(e.target.value)}
            />

            <span className="absolute inset-y-0 end-0 grid w-10 place-content-center">
              <button type="button"
                className="text-gray-600 hover:text-gray-700">
                <span className="sr-only">Search</span>

                <svg
                  xmlns="http://www.w3.org/2000/svg"
                  fill="none"
                  viewBox="0 0 24 24"
                  strokeWidth="1.5"
                  stroke="currentColor"
                  className="h-4 w-4"
                >
                  <path
                    strokeLinecap="round"
                    strokeLinejoin="round"
                    d="M21 21l-5.197-5.197m0 0A7.5 7.5 0 105.196 5.196a7.5 7.5 0 0010.607 10.607z"
                  />
                </svg>
              </button>
            </span>
          </div>
        </div>
      </section>

      <div className="max-w-screen-2xl px-4 py-8 mx-auto -mt-6 sm:px-6 sm:py-12 lg:px-8">
        {(isLoading) && <div className="w-full text-center"><div className="h-1/4 w-1/4  loading loading-bars"></div></div>}
        <div className="lg:mt-0 mt-24">
          <ul className="grid gap-4 mt-5 sm:grid-cols-2 lg:grid-cols-3">
            {data && data.map((item) => <MenuItem item={item} key={item.id} />)}
          </ul>
        </div>
      </div>
      <div className="w-full text-center">
        <button className="btn  hover:bg-gray-700 text-white font-bold mx-1 my-3  focus:outline-black focus:ring active-gray-300 rounded-none "
          onClick={() => {
            setIndex(index - 3)
          }}
          disabled={
            index === 0
          }
        >
          <GrFormPrevious size={24} />
        </button>
        <button className="btn hover:bg-gray-700 text-white font-bold mx-1  my-3  focus:outline-black focus:ring active-gray-300 rounded-none "
          onClick={() => {
            if (!isPreviousData && data) {
              setIndex(index + 3)
            }
          }}
          disabled={isPreviousData || (data && data.length < 3)}
        >
          <GrFormNext size={24} />
        </button>
      </div>
    </div>
  </>)
}

function MenuItem({ item }: { item: typeViewProductData }) {
  const { data: imageUrl } = useQuery(['getItemPicture', item.imageUrl], () => getItemPicture(item.imageUrl))
  return (<>
    <li>
      <div className="group relative block"> <div className="relative h-[350px] sm:h-[450px]">
        <img
          src={imageUrl && `${imageUrl}?dummy=${crypto.randomUUID()}`}
          alt="loading"
          className="h-[350px] w-full object-cover transition duration-500 group-hover:scale-105 sm:h-[450px]"
        />
      </div>
        <div className="absolute inset-0 flex flex-col items-start justify-end p-6 ">
          <OrderModal item={item} />
        </div>
      </div>
      <div>
        <h3 className="text-xl font-medium text-black hover:underline p-3  uppercase text-center">{item.name}</h3>
        <p className="mt-1.5 max-w-[40ch] text-sm text-black uppercase">
          {item.description}
        </p>
        <p className="mt-1.5 max-w-[40ch] text-lg text-black">
          <BiRupee className="inline" />: {item.price}
        </p>
        <p className="mt-1.5 max-w-[40ch] text-lg text-black">
          Size: {item.size}
        </p>
      </div>
    </li >
  </>)
}

function OrderModal({ item }: { item: typeViewProductData }) {
  const { data: session } = useQuery(['getSession'], getSession)
  const userId = session?.user.id
  const userName = session?.user.user_metadata.firstName
  const [quantity, setQuantity] = useState<"quantity" | number>(1)
  const [price, setPrice] = useState(item.price)
  const [addressId, setAddressId] = useState('')
  const { data: userAddresses } = useQuery(['getAddresses', userId], () => getAddressDetaials({ customerId: userId }))
  const { mutate: orderItemFn, isLoading: isOrdring } = useMutation(['createOrder'], orderItem, {
    onSuccess: () => {
      if (addressId !== "") toast.success('Order placed successfully')
      setQuantity(1);
      setAddressId('');
    },
    onError: () => {
      toast.error('Something went wrong')
    }
  })

  return (<>
    <label htmlFor={item.id} className="btn-outline mt-3 inline-block bg-black text-xs font-medium uppercase tracking-wide border border-white p-3 text-white">Order now</label>
    <input type="checkbox" id={item.id} className="modal-toggle" />
    <div className="modal">
      <div className="modal-box rounded-none">
        <h3 className="font-bold text-lg">Order Now</h3>
        <div className="flex flex-col gap-2 ">
          <p className="mt-1.5 max-w-[40ch] text-black text-sm">
            Item : <span className="uppercase text-lg font-bold">{item.name}</span>
          </p>
          <p className="mt-1.5 max-w-[40ch] text-black text-sm">
            Price : <span className="uppercase ">{price}</span>
          </p>
          <div>
            <div className="inline mr-4 text-lg">Quantity</div>
            <input type="number" placeholder="Quantity" value={quantity}
              onChange={(e) => {
                if (Number(e.target.value) == 0) {
                  setQuantity('quantity')
                  setPrice(0)
                }
                if (Number(e.target.value) > 0) {
                  setQuantity(Number(e.target.value))
                  setPrice(Number(e.target.value) * item.price)
                }
              }}
              className="input input-bordered w-3/4" />
          </div>
          <div>
            <div className="inline mr-5 text-lg">Address</div>
            <select className="w-full rounded-md border-gray-200" onChange={(e) => { setAddressId(e.target.value) }}>
              <option></option>
              {userAddresses && userAddresses.map((address) => (<option value={address.id} key={address.id}>{address.addressName}</option>))}
            </select>
          </div>
          <div>
          </div>
        </div>
        <Link to={'/addressdetails'} className="btn-outline inline-block bg-black text-xs font-medium uppercase tracking-wide border border-white p-3 text-white">Add Address</Link>
        <div className="modal-action">
          <label htmlFor={item.id} className="btn">Close!</label>
          {session ? (<button className={`${isOrdring ? "loading loading-spinner" : ""}btn-outline inline-block bg-black text-xs font-medium uppercase tracking-wide border border-white p-3 text-white`}
            onClick={() => {
              orderItemFn({ item: item, quantity: quantity, customerAddressDetailsId: addressId, userName: userName })
            }}
          >Place Order</button>) :
            (<button
              className={`${isOrdring ? "loading loading-spinner" : ""}btn-outline inline-block bg-black text-xs font-medium uppercase tracking-wide border border-white p-3 text-white`}
              onClick={() => { toast.error('Please login to place order') }}
            >Place Order</button>)}
        </div>
      </div>
    </div>
  </>)
}
