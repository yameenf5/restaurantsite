import { toast } from "react-hot-toast";
import { supabaseClient } from "../../utils/supabaseClient";

export async function getOnlineUncompletedOrders() {
  const { data, error } = await supabaseClient.from('Orders').select('*, CustomerDetails (*) ').match(
    { orderType: 'ONLINE', isCancled: false, paid: false }
  )
  if (error) throw error
  if (data) return data
}

export async function confirmOrder({ orderId, isConfirmed }: { orderId: string, isConfirmed: boolean }) {
  if (isConfirmed) {
    toast.error("Already Confirmed");
  }
  const { error } = await supabaseClient.from('Orders').update({ isConfirmed: true }).eq('id', orderId);
  if (error) throw error
}

export async function completeOrder({ orderId, IsPaid, isConfirmed }: { orderId: string, IsPaid: boolean, isConfirmed: boolean }) {
  if (isConfirmed === false) {
    toast.error("Order is not confirmed");
    return
  }
  if (IsPaid) {
    toast.error("Already paid");
  }
  const { error } = await supabaseClient.from('Orders').update({ paid: true }).eq('id', orderId);
  if (error) throw error
}

export async function cancelOrder({ orderId, IsPaid }: { orderId: string, IsPaid: boolean }) {
  if (IsPaid) {
    toast.error("Already complete Order");
  }
  const { error } = await supabaseClient.from('Orders').update({ 'isCancled': true }).eq('id', orderId);
  if (error) throw error
}
