import { useMutation, useQuery, useQueryClient } from "@tanstack/react-query";
import { ColumnDef, Row } from "@tanstack/react-table";
import { typeAddressDetailsView, typeOrderWithAddress } from "../../../types";
import { OnlineOrdersTable } from "../../components/Tables/OnlineOrderTable";
import { toast } from "react-hot-toast";
import Navbar from "../../components/Navbar";
import { cancelOrder, completeOrder, confirmOrder, getOnlineUncompletedOrders } from "./handleOnlineOrder";

export function UncompletedOrders() {
  const { data, isLoading: isfetching } = useQuery(['getOnlineUncompletedOrders'], getOnlineUncompletedOrders)
  const column: ColumnDef<typeOrderWithAddress>[] = [
    {
      header: 'Customer Name',
      accessorKey: 'customerName'
    },
    {
      header: 'Product Name',
      accessorKey: 'item'
    },
    {
      header: 'Price In Rupees',
      accessorKey: 'price'
    },
    {
      header: 'Confirmed',
      accessorFn: (row) => row.isConfirmed ? 'Confirmed' : 'Not Confirmed'
    },
    {
      header: 'Paid',
      accessorFn: (row) => row.paid ? 'Paid' : 'Not Paid'
    },
    {
      header: 'Size',
      accessorKey: 'size'
    },
    {
      header: 'Order Type',
      accessorKey: 'orderType'
    },
    {
      header: 'quantity',
      accessorKey: 'quantity'
    },
    {
      header: 'Add-ons',
      accessorFn: (row) => row.addons ? row.addons : 'No Addons'
    },
    {
      header: 'cancelled',
      accessorFn: (row) => row.isCancled ? 'Cancelled' : 'NO'
    },
    {
      header: 'Action',
      cell: ({ row }) => {
        return (<>
          <ActionButtons dataRow={row} />
        </>
        )
      }
    }
  ]

  if (isfetching) return <div className="w-full text-center"><div className="h-1/4 w-1/4  loading loading-bars"></div></div>
  if (data)
    return (<>
      <Navbar />
      <OnlineOrdersTable dataArray={data} column={column} />
    </>)
}

function ActionButtons({ dataRow }: { dataRow: Row<typeOrderWithAddress> }) {
  const queryClient = useQueryClient()
  const { mutate: completeOrderFn, isLoading: isCompleteing } = useMutation(['completingOrder'], completeOrder, {
    onSuccess: () => queryClient.invalidateQueries(['getOnlineUncompletedOrders']),
    onError: () => toast.error("INTERNAL SERVER ERROR")
  })

  const { mutate: confirmOrderFn, isLoading: confriming } = useMutation(['completingOrder'], confirmOrder, {
    onSuccess: () => queryClient.invalidateQueries(['getOnlineUncompletedOrders']),
    onError: () => toast.error("INTERNAL SERVER ERROR")
  })

  const { mutate: cancleOrderFn, isLoading: canceling } = useMutation(['completingOrder'], cancelOrder, {
    onSuccess: () => queryClient.invalidateQueries(['getOnlineUncompletedOrders']),
    onError: () => toast.error("INTERNAL SERVER ERROR")
  })

  return (<>
    <div className="flex flex-row justify-start">
      <button className={`${confriming ? "loading loading-spinner" : ""}  btn bg-black hover:bg-gray-700 text-white font-bold mx-1  -my-2 focus:outline-black focus:ring active-gray-300 rounded-none text-xs`}
        onClick={() => confirmOrderFn({ orderId: dataRow.original.id, isConfirmed: dataRow.original.isConfirmed })}
      >
        Confirm
      </button>
      <button className={`${isCompleteing ? "loading loading-spinner" : ""}  btn bg-black hover:bg-gray-700 text-white font-bold mx-1  -my-2 focus:outline-black focus:ring active-gray-300 rounded-none text-xs`}
        onClick={() => completeOrderFn({ orderId: dataRow.original.id, IsPaid: dataRow.original.paid, isConfirmed: dataRow.original.isConfirmed })}
      >
        Paid
      </button>
      <button className={`${canceling ? "loading loading-spinner" : ""}  btn bg-black hover:bg-gray-700 text-white font-bold mx-1  -my-2 focus:outline-black focus:ring active-gray-300 rounded-none text-xs`}
        onClick={() => cancleOrderFn({ orderId: dataRow.original.id, IsPaid: dataRow.original.paid })}
      >
        cancel
      </button>
      {dataRow.original.CustomerDetails && <OrderaddressModal addressDetails={dataRow.original.CustomerDetails} orderId={dataRow.original.id} />}
    </div>
  </>)
}


function OrderaddressModal({ addressDetails, orderId}: { addressDetails: typeAddressDetailsView, orderId:string }) {
  return <>
    <label htmlFor={orderId} className=" btn bg-black hover:bg-gray-700 text-white font-bold mx-1  -my-2 focus:outline-black focus:ring active-gray-300 rounded-none text-xs">      Address 
    </label>
    <input type="checkbox" id={orderId} className="modal-toggle" />
    <div className="modal">
      <div className="modal-box">
        <h3 className="font-bold text-lg">Address To Deliver</h3>
        <p className="py-4">Area : {addressDetails.area}</p>
        <p className="py-4">City : {addressDetails.city}</p>
        <p className="py-4">Landmark : {addressDetails.landmark}</p>
        <p className="py-4">Pincode : {addressDetails.pincode}</p>
        <p className="py-4">House No : {addressDetails.houseNo}</p>
        <div className="modal-action">
          <label htmlFor={orderId} className="btn">Close!</label>
        </div>
      </div>
    </div>
  </>
}


