import { useMutation, useQuery } from "@tanstack/react-query";
import { getProducts } from "../HandleProduct/handleProduct";
import { ColumnDef, Row } from "@tanstack/react-table";
import { typeMenuProductEdit, typeOrderInsert, typeViewProductData } from "../../../types";
import { AiOutlineWarning } from "react-icons/ai";
import { SubmitHandler, useForm } from "react-hook-form";
import { toast } from "react-hot-toast";
import { createOrder } from "./handleCreateOrder";
import { ProductTable } from "../../components/Tables/ProductTable";
import Navbar from "../../components/Navbar";

export function CreateOrder() {
  const { data, isLoading: isfetching } = useQuery(['getProducts'], getProducts)
  const column: ColumnDef<typeViewProductData>[] = [
    {
      header: 'Product Name',
      accessorKey: 'name'
    },
    {
      header: 'Category',
      accessorKey: 'category'
    },
    {
      header: 'Price In Rupees',
      accessorKey: 'price'
    },
    {
      header: 'Available',
      accessorFn: (row) => row.isAvailable ? 'Yes' : 'No'
    },
    {
      header: 'Size',
      accessorKey: 'size'
    },
    {
      header: 'Action',
      cell: ({ row }) => {
        return (<>
          <AddOrderButton row={row} />
        </>
        )
      }
    }
  ]

  if (isfetching) return <div className="w-full text-center"><div className="h-1/4 w-1/4  loading loading-bars"></div></div>
  if (data)
    return (<>
      <Navbar />
      <ProductTable dataArray={data} column={column} />
    </>)
}



function AddOrderButton({ row }: { row: Row<typeViewProductData> }) {
  return <>
    <div className="flex flex-row justify-start">
      <label htmlFor={row.original.id} className="">
        {row.original.isAvailable && <div className="btn bg-black hover:bg-gray-700 text-white font-bold mx-1  -my-2 focus:outline-black focus:ring active-gray-300 rounded-none "
        >
          Order
        </div>}
      </label>
      {!row.original.isAvailable && <button className="btn bg-black hover:bg-gray-700 text-white text-sm font-bold mx-1 -my-2 py-0 focus:outline-black focus:ring active-gray-300 rounded-none "
        disabled={true}
      >
        Order
      </button>}
      <input type="checkbox" id={row.original.id} className="modal-toggle" />
      <div className="modal">
        <div className="modal-box">
          <CreateOrderForm item={row.original} />
          <div className="modal-action">
            <label htmlFor={row.original.id} className="btn">Close</label>
          </div>
        </div>
      </div>
    </div>
  </>
}


function CreateOrderForm({ item }: { item: typeMenuProductEdit }) {
  const { mutate: createOrderFn, isLoading: isCreating } = useMutation(['createOrderInShop'], createOrder, {
    onSuccess: () => { toast.success("order created") },
    onError: () => { toast.error("INTERNAL SERVER ERROR") }
  })
  const { register, handleSubmit, formState: { errors } } = useForm<typeOrderInsert>({
    defaultValues: {
      item: item.name,
      size: item.size,
      itemId: item.id,
      price: (item.price)
    }
  });

  const onSubmit: SubmitHandler<typeOrderInsert> = (fdata) => {
    createOrderFn({ order: fdata, isAdmin: true, })
  };
  return (<>
    <section className="">
      <div className="">
        <form onSubmit={handleSubmit(onSubmit)} className="grid grid-cols-3">
          <div className="col-span-3">
            <label htmlFor="CustomerName" className="block text-sm font-medium text-gray-700">
              CustomerName
            </label>
            {errors.customerName && <span className="text-yellow-300 flex"><AiOutlineWarning className="m-1" /> {errors.customerName?.message}</span>}
            <input
              type="text"
              id="CustomerName"
              className="mt-1 w-full rounded-md border-gray-200 bg-white text-sm text-gray-700 shadow-sm"
              {...register("customerName", { required: "Name is required" })}
            />
          </div>
          <div className="col-span-3 ">
            <label
              htmlFor="item"
              className="block text-sm font-medium text-gray-700"
            >
              Item
            </label>
            <input
              type="text"
              id="item"
              className="mt-1 w-full rounded-md border-gray-200 bg-white text-sm text-gray-700 shadow-sm"
              {...register("item")}
            />
          </div>
          <div className="col-span-3">
            <label
              htmlFor="price"
              className="block text-sm font-medium text-gray-700"
            >
              Price
            </label>
            <input
              type="number"
              id="price"
              className="mt-1 w-full rounded-md border-gray-200 bg-white text-sm text-gray-700 shadow-sm"
              {...register("price")}
            />
          </div>
          <div className="col-span-3">
            <label
              htmlFor="quantity"
              className="block text-sm font-medium text-gray-700"
            >
              Quantity
            </label>
            {errors.quantity && <span className="text-yellow-300 flex"><AiOutlineWarning className="m-1" /> {errors.quantity?.message}</span>}
            <input
              type="number"
              id="price"
              className="mt-1 w-full rounded-md border-gray-200 bg-white text-sm text-gray-700 shadow-sm"
              {...register("quantity", { required: "quantity is required" })}
            />
          </div>

          <div className="col-span-3">
            <label
              htmlFor="addons"
              className="block text-sm font-medium text-gray-700"
            >
            Addons
            </label>
            {errors.quantity && <span className="text-yellow-300 flex"><AiOutlineWarning className="m-1" /> {errors.quantity?.message}</span>}
            <input
              type="text"
              id="addons"
              className="mt-1 w-full rounded-md border-gray-200 bg-white text-sm text-gray-700 shadow-sm"
              {...register("addons" )}
            />
          </div>


          <div className="col-span-3">
            <label
              htmlFor="OrderType"
              className="block text-sm font-medium text-gray-700"
            >
              OrderType
            </label>
            <select className="w-full rounded-md border-gray-200" {...register("orderType")}>
              <option value={"TAKEAWAY"}>Take away</option>
              <option value={"ATRESTRAUANT"}>At restaurant</option>
            </select>
          </div>

          <div className="col-span-3 ">
            <label
              htmlFor="size"
              className="block text-sm font-medium text-gray-700"
            >
              Size
            </label>
            <select className="w-full rounded-md border-gray-200" {...register("size")}>
              <option value={"N/A"}>N/A</option>
              <option value={"Small"}>Small</option>
              <option value={"Medium"}>Medium</option>
              <option value={"Large"}>Large</option>
            </select>
          </div>
          <button className={`${isCreating ? "loading loading-spinner" : ""}  btn bg-black mt-3 hover:bg-gray-700 text-white font-bold focus:outline-black focus:ring active-bg-gray-400 rounded-none`}>
            Create
          </button>
        </form>
      </div>
    </section>
  </>)
}


