import Reactlogo from "../../assets/react.svg"
import { Link, useNavigate } from "react-router-dom";
import { useForm, SubmitHandler } from "react-hook-form"
import { AiOutlineWarning } from "react-icons/ai"
import type { typeUser } from "../../../types/index"
import { createUserWithPasswordCheck } from "./signUp";
import { useMutation } from "@tanstack/react-query";
import { toast } from "react-hot-toast";
import Navbar from "../../components/Navbar";


export function SignUp() {
  const navigate = useNavigate();
  const { mutate: createUser, isLoading } = useMutation({
    mutationFn: createUserWithPasswordCheck,
    onSuccess: () => navigate('/'),
    onError: () => toast.error('password does not match')
  })

  const { register, handleSubmit, formState: { errors }, } = useForm<typeUser>();
  const onSubmit: SubmitHandler<typeUser> = (data) => {
    try {
      createUser(data)
    } catch (error) {
      toast.error("Password dost not match")
    }
  };

  return (<>
    <Navbar />
    <section className="bg-white h-screen">
      <div className="lg:grid lg:min-h-screen lg:grid-cols-12">
        <section
          className="relative flex h-32 items-end bg-gray-900 lg:col-span-5 lg:h-full xl:col-span-6"
        >
          <img
            alt="Night"
            src="https://images.unsplash.com/photo-1540189549336-e6e99c3679fe?ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D&auto=format&fit=crop&w=1974&q=80"
            className="absolute inset-0 h-full w-full object-cover opacity-80"
          />

          <div className="hidden lg:relative lg:block lg:p-12">
            <h2 className="mt-6 text-2xl font-bold text-white sm:text-3xl md:text-4xl">
              Welcome to JUST EAT{/*TODO*/}
            </h2>

            <p className="mt-4 leading-relaxed text-white">
              We provide the best and fresh food.
            </p>
          </div>
        </section>

        <main
          className="flex items-center justify-center px-8 py-8 sm:px-12 lg:col-span-7 lg:px-16 lg:py-12 xl:col-span-6"
        >
          <div className="max-w-xl lg:max-w-3xl">
            <div className="relative -mt-16 block lg:hidden">
              <a
                className="inline-flex h-16 w-16 items-center justify-center rounded-full bg-white text-blue-600 sm:h-20 sm:w-20"
                href="/"
              >
                <span className="sr-only">Home</span>
                <img src={Reactlogo} />
              </a>

              <h1
                className="mt-2 text-2xl font-bold text-gray-900 sm:text-3xl md:text-4xl"
              >
                Welcome to JUST EAT{/*TODO*/}
              </h1>
              <p className="mt-4 leading-relaxed text-gray-500">
                We provide the best and fresh food.
              </p>
            </div>
            <form onSubmit={handleSubmit(onSubmit)} className="mt-8 grid grid-cols-6 gap-6">
              <div className="col-span-6 sm:col-span-3">
                <label
                  htmlFor="FirstName"
                  className="block text-sm font-medium text-gray-700"
                >
                  First Name
                </label>
                {errors.firstName && <span className="text-yellow-300 flex"> <AiOutlineWarning className="m-1" /> {errors.firstName?.message}</span>}
                <input
                  type="text"
                  id="FirstName"
                  className="mt-1 w-full rounded-md border-gray-200 bg-white text-sm text-gray-700 shadow-sm"
                  {...register("firstName", { required: "First Name is required", maxLength: { message: "First Name is too long", value: 15 } })}
                />
              </div>

              <div className="col-span-6 sm:col-span-3">
                <label
                  htmlFor="LastName"
                  className="block text-sm font-medium text-gray-700"
                >
                  Last Name
                </label>
                {errors.lastName && <span className="text-yellow-300 flex"><AiOutlineWarning className="m-1" /> {errors.lastName?.message}</span>}
                <input
                  type="text"
                  id="LastName"
                  className="mt-1 w-full rounded-md border-gray-200 bg-white text-sm text-gray-700 shadow-sm"
                  {...register("lastName", { required: "Last Name is required", maxLength: { message: "Last Name is too long", value: 15 } })}
                />
              </div>

              <div className="col-span-6">
                <label htmlFor="Email" className="block text-sm font-medium text-gray-700">
                  Email
                </label>
                {errors.email && <span className="text-yellow-300 flex"><AiOutlineWarning className="m-1" /> {errors.email?.message}</span>}
                <input
                  type="email"
                  id="Email"
                  className="mt-1 w-full rounded-md border-gray-200 bg-white text-sm text-gray-700 shadow-sm"
                  {...register("email", { required: "Email is require" })}
                />
              </div>

              <div className="col-span-6 sm:col-span-3">
                <label
                  htmlFor="Password"
                  className="block text-sm font-medium text-gray-700"
                >
                  Password
                </label>
                {errors.password && <span className="text-yellow-300 flex"><AiOutlineWarning className="m-1" /> {errors.password?.message}</span>}
                <input
                  type="password"
                  id="Password"
                  className="mt-1 w-full rounded-md border-gray-200 bg-white text-sm text-gray-700 shadow-sm"
                  {...register("password", {
                    required: "Password is required",
                    maxLength: { message: "Password is too long", value: 15 },
                    minLength: { message: "Password is too small", value: 3 }
                  })}
                />
              </div>
              <div className="col-span-6 sm:col-span-3">
                <label
                  htmlFor="PasswordConfirmation"
                  className="block text-sm font-medium text-gray-700"
                >
                  Password Confirmation
                </label>
                {errors.PasswordConfirmation && <span className="text-yellow-300 flex"><AiOutlineWarning className="m-1" /> {errors.PasswordConfirmation?.message}</span>}
                <input
                  type="password"
                  id="PasswordConfirmation"
                  className="mt-1 w-full rounded-md border-gray-200 bg-white text-sm text-gray-700 shadow-sm"
                  {...register("PasswordConfirmation", {
                    required: "Password is required",
                    maxLength: { message: "Password is too long", value: 15 },
                    minLength: { message: "Password is too small", value: 3 }
                  })}
                />
              </div>
              <div className="col-span-6 sm:flex sm:items-center sm:gap-4">
                <button className="inline-block shrink-0  border border-white bg-black 
                px-12 py-3 text-sm font-medium text-white transition focus:outline-none focus:ring active:text-blue-500" >
                  <div className={`${isLoading ? "loading loading-spinner" : ""} mx-2  `}></div>
                  Create an account
                </button>
                <p className="mt-4 text-black sm:mt-0">
                  Already have an account?
                  <Link className="underline text-lg text-blue-500" to={'/signIn'}>Log in</Link>.
                </p>
              </div>
            </form>
          </div>
        </main>
      </div>
    </section>
  </>)
}
