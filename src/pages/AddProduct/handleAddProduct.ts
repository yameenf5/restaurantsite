import { toast } from "react-hot-toast";
import type { typeAddProduct } from "../../../types";
import { v4 as uuidv4 } from "uuid";
import { supabaseClient } from "../../utils/supabaseClient";


export async function addProduct({ name, category, price, description, isAvailable, image , size}: typeAddProduct) {
  const { data: createdProduct, error } = await supabaseClient.from('MenuProduct').insert([{
    name: name,
    category: category,
    price: price,
    description: description,
    isAvailable: isAvailable,
    size: size
  }]).select()

  const { data: uploadedImage, error: storageErr } = await supabaseClient.storage
    .from('Media')
    .upload(`ProductImages/${uuidv4()}`, image[0], {
      cacheControl: '3600',
      upsert: false,
    });

  if (error) toast.error(error.message)
  if (storageErr) toast.error(storageErr.message)

  if (createdProduct === null) return null;
  const { error: updateErr } = await supabaseClient.from('MenuProduct').update({ imageUrl: uploadedImage?.path }).eq('id', createdProduct[0].id);
  if (updateErr) toast.error(updateErr.message)

  if (createdProduct) return createdProduct[0].id
}

export async function getProduct(id: string) {
  if (id === "") return null;
  const { data, error } = await supabaseClient.from('MenuProduct').select('*').eq('id', id);
  if (error) toast.error(error.message);
  if (data) return data[0].imageUrl;
}

