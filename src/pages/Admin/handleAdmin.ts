import { toast } from "react-hot-toast";
import { typeCategoriesInsert } from "../../../types";
import { supabaseClient } from "../../utils/supabaseClient";

export async function deleteCategory(id: string) {
  const { error } = await supabaseClient.from('Categories').delete().eq('id', id);
  if (error) throw error;
}

export async function addCategory(category: typeCategoriesInsert) {
  const { error } = await supabaseClient.from('Categories').insert(category);
  if (error) throw error;
}

export async function completedOrderCount() {
  const { count } = await supabaseClient.from('Orders').select('*', { count: 'exact', head: true }).eq('paid', true);
  if (count) return count;
}

export async function NewOnlineOrderCount() {
  const { count } = await supabaseClient.from('Orders').select('*', { count: 'exact', head: true })
    .match({ isConfirmed: false, isCancled: false, orderMode: 'ONLINE' });
  if (count) return count
}

export async function totalItemsCount() {
  const { data } = await supabaseClient.from('MenuProduct').select('*').eq('isDeleted', false);
  if (data) {
    let totalAvailableItems = 0;
    data.forEach((item) => {
      if (item.isAvailable)
        totalAvailableItems++;
    })
    return { totalAvailableItems, totalItems: data.length }
  }
}


export async function totalOrders() {
  const { count } = await supabaseClient.from('Orders').select('*', { count: 'exact', head: true });
  if (count) return count;
}

export async function revenuGenerated() {
  const { data } = await supabaseClient.from('Orders').select('price, orderMode').eq('paid', true);
  if (data) {
    let totalInshopRevenue = 0;
    let totalOnlineRevenue = 0;
    data.forEach((order) => {
      if (order.orderMode === 'ONLINE') {
        totalOnlineRevenue += order.price;
      }
      else {
        totalInshopRevenue += order.price;
      }
    })
    return { totalInshopRevenue, totalOnlineRevenue, totalRevenue: totalInshopRevenue + totalOnlineRevenue };
  }
}

export async function getCompletedOrders() {
  const { data } = await supabaseClient.from('Orders').select('*').eq('paid', true);
  if (data) return data;
}

export async function getFeaturedItem() {
  const { data } = await supabaseClient.from('MenuProduct').select('*').eq('isfeatured', true);
  if (data) return data
}

export async function unFeature({ id }: { id: string }) {
  const { error } = await supabaseClient.from('MenuProduct').update({ isfeatured: false, isLargePictureFeatured: false }).eq('id', id);
  if (error) throw error;
}

export async function toggleHero({ id, isHero }: { id: string, isHero: boolean },) {
  const { count } = await supabaseClient.from('MenuProduct').select('*', { count: "exact", head: true }).eq('isLargePictureFeatured', true);
  if (count && (count === 1) && !isHero) {
    toast.error('You can only feature 1 item at a time');
    return
  }
  const { error } = await supabaseClient.from('MenuProduct').update({ isLargePictureFeatured: !isHero }).eq('id', id);
  if (error) throw error;
}

export async function feature({ id }: { id: string }) {
  const { count } = await supabaseClient.from('MenuProduct').select('*', { count: "exact", head: true }).eq('isfeatured', true);
  if (count) {
    if (count === 3) {
      toast.error('You can only feature 3 items at a time')
      return count
    }
  }
  const { error } = await supabaseClient.from('MenuProduct').update({ isfeatured: true,  }).eq('id', id);
  if (error) throw error;
  toast.success('Item featured successfully')
}

export async function getUnFeaturedItems() {
  const { data } = await supabaseClient.from('MenuProduct').select('*').match({isfeatured:false, isDeleted: false });
  if (data) return data;
}
