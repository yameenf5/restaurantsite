import { toast } from "react-hot-toast";
import { typeMenuProductEdit } from "../../../types";
import { supabaseClient } from "../../utils/supabaseClient";
import { v4 as uuidv4 } from 'uuid';

export async function getProducts() {
  const { data, error } = await supabaseClient.from('MenuProduct').select('*').match({ isDeleted: false });
  if (data === null) throw error;
  return data
}

export async function updateProduct(updatedDetails: typeMenuProductEdit) {
  const { error } = await supabaseClient.from('MenuProduct')
    .update(updatedDetails).eq('id', updatedDetails.id).select();
  if (error) throw error;
}

export async function getProductPicture(imageUrl: string) {
  if (imageUrl === "NOTUPLOADED" || imageUrl === "UPLOADFAILED") return "NOTUPLOADED"
  const { data } = supabaseClient.storage.from('Media').getPublicUrl(imageUrl);
  if (imageUrl === "NOTUPLOADED" || imageUrl === "UPLOADFAILED") return "NOTUPLOADED"
  return data.publicUrl;
}

export async function reUploadImage({ image, imagePath, id }: { image: File, imagePath: string, id: string }) {
  if (imagePath === "UPLOADFAILED" || imagePath === "NOTUPLOADED") {
    const { data: uploadedImage, error: storageErr } = await supabaseClient.storage
      .from('Media')
      .upload(`ProductImages/${uuidv4()}`, image, {
        cacheControl: '3600',
        upsert: false,
      });
    if (storageErr) toast.error(storageErr.message)
    const { error: updateErr } = await supabaseClient.from('MenuProduct').update({ imageUrl: uploadedImage?.path }).eq('id', id);
    if (updateErr) toast.error(updateErr.message)
  } else {
    const { error } = await supabaseClient.storage.from('Media').update(imagePath, image);
    if (error) throw error;
  }
}

export async function deleteProduct({ id, imagePath }: { id: string, imagePath: string }) {
  const { error } = await supabaseClient.from('MenuProduct').update({ isDeleted: true, imageUrl: "NOTUPLOADED", isfeatured: false, isLargePictureFeatured:false }).eq('id', id);
  if (error) throw error
  const { error: imageRemovalError } = await supabaseClient.storage.from('Media').remove([imagePath]);
  if (imageRemovalError) throw imageRemovalError
}

export async function getCategories() {
  const { data, error } = await supabaseClient.from('Categories').select('*');
  if (error) throw error;
  return data;
}




