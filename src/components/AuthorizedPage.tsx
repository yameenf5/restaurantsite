import { Link } from "react-router-dom";

export function AuthorizedPage() {

  return (
    <div
      className="
    flex
    items-center
    justify-center
    w-screen
    h-screen
    bg-black
  "
    >
      <div className="px-40 py-20 bg-white rounded-md shadow-xl">
        <div className="flex flex-col items-center">
          <h6 className="mb-2 text-2xl font-bold text-center text-gray-800 md:text-3xl">
            <span className="text-red-500">Oops!</span>You are alreay authorized.
          </h6>
          <p className="mb-8 text-center text-gray-500 md:text-lg">
          </p>
          <Link
            to={'/'}
            className="px-6 py-2 text-sm font-semibold text-blue-800 bg-blue-100"
          >
            Go home
          </Link>
        </div>
      </div>
    </div>
  )
}
