import { Link } from "react-router-dom"
import ReactLogo from "../../assets/react.svg"
import { useMutation, useQuery, useQueryClient } from "@tanstack/react-query"
import { typeViewProductData } from "../../../types"
import { feature, getUnFeaturedItems } from "../../pages/Admin/handleAdmin"
import { toast } from "react-hot-toast"

export function SideBar({ userName }: { userName: string }) {
  return (<>
    <div className="w-1/5 lg:block hidden -mr-1" ><SideBarComponent userName={userName} /></div>
    <div className="lg:hidden"><SideBarSmScreen userName={userName} /> </div>
  </>)
}

function SideBarComponent({ userName }: { userName: string }) {
  return <>
    <div className="flex h-screen flex-col justify-between border-e bg-gray-800 w-full /">
      <div className="px-4 py-6 flex-1">
        <Link to={'/'}>  <img className="w-10 cursor-pointer rounded-full bg-white" src={ReactLogo} alt="logo" /></Link>
        <ul className="mt-6 space-y-1">
          <li>
            <a
              href=""
              className="block rounded-lg bg-gray-100 px-4 py-2 text-sm font-medium text-black"
            >
              Admin
            </a>
          </li>

          <li>
            <details className="group [&_summary::-webkit-details-marker]:hidden">
              <summary
                className="flex cursor-pointer items-center justify-between rounded-lg px-4 py-2 text-white hover:bg-gray-100 hover:text-gray-700"
              >
                <span className="text-sm font-medium"> Menu </span>

                <span
                  className="shrink-0 transition duration-300 group-open:-rotate-180"
                >
                  <svg
                    xmlns="http://www.w3.org/2000/svg"
                    className="h-5 w-5"
                    viewBox="0 0 20 20"
                    fill="currentColor"
                  >
                    <path
                      fillRule="evenodd"
                      d="M5.293 7.293a1 1 0 011.414 0L10 10.586l3.293-3.293a1 1 0 111.414 1.414l-4 4a1 1 0 01-1.414 0l-4-4a1 1 0 010-1.414z"
                      clipRule="evenodd"
                    />
                  </svg>
                </span>
              </summary>

              <ul className="mt-2 space-y-1 px-4">
                <li>
                  <Link
                    className="block rounded-lg px-4 py-2 text-sm font-medium text-white hover:bg-gray-100 hover:text-gray-700"
                    to={"/admin/additem"}
                  >
                    Add Item
                  </Link>
                </li>
                <li>
                  <Link
                    className="block rounded-lg px-4 py-2 text-sm font-medium text-white hover:bg-gray-100 hover:text-gray-700"
                    to={"/admin/handleitem"}
                  >
                    Handle Menu
                  </Link>
                </li>
              </ul>
            </details>
          </li>
          <li>
            <details className="group [&_summary::-webkit-details-marker]:hidden">
              <summary
                className="flex cursor-pointer items-center justify-between rounded-lg px-4 py-2 text-white hover:bg-gray-100 hover:text-gray-700"
              >
                <span className="text-sm font-medium"> Order </span>

                <span
                  className="shrink-0 transition duration-300 group-open:-rotate-180"
                >
                  <svg
                    xmlns="http://www.w3.org/2000/svg"
                    className="h-5 w-5"
                    viewBox="0 0 20 20"
                    fill="currentColor"
                  >
                    <path
                      fillRule="evenodd"
                      d="M5.293 7.293a1 1 0 011.414 0L10 10.586l3.293-3.293a1 1 0 111.414 1.414l-4 4a1 1 0 01-1.414 0l-4-4a1 1 0 010-1.414z"
                      clipRule="evenodd"
                    />
                  </svg>
                </span>
              </summary>

              <ul className="mt-2 space-y-1 px-4">
                <li>
                  <Link
                    className="block rounded-lg px-4 py-2 text-sm font-medium text-white hover:bg-gray-100 hover:text-gray-700"
                    to={"/admin/createorder"}
                  >
                    Create Order
                  </Link>
                </li>
                <li>
                  <Link
                    className="block rounded-lg px-4 py-2 text-sm font-medium text-white hover:bg-gray-100 hover:text-gray-700"
                    to={"/admin/inshoporder"}
                  >
                    In Shop Order
                  </Link>
                </li>

                <li>
                  <Link
                    className="block rounded-lg px-4 py-2 text-sm font-medium text-white hover:bg-gray-100 hover:text-gray-700"
                    to={"/admin/onlineOrders"}
                  >
                    Online Orders
                  </Link>
                </li>
                <li>
                  <Link
                    to={'/admin/handleOnlineOrders'}
                    className="block rounded-lg px-4 py-2 text-sm font-medium text-white hover:bg-gray-100 hover:text-gray-700"
                  >
                    Handle Online Orders
                  </Link>
                </li>
              </ul>
            </details>
          </li>
          <li>
            <EditFeaturedItemModal />
          </li>
        </ul>
      </div>

      <div className="sticky inset-x-0 bottom-0 border-t border-gray-100">
        <a href="#" className="flex items-center gap-2 bg-white p-4 hover:bg-gray-50">
          <img className="w-10 cursor-pointer rounded-full bg-white" src={ReactLogo} alt="logo" />
          <div>
            <p className="text-xs">
              <strong className="block font-medium uppercase">{userName}</strong>
            </p>
          </div>
        </a>
      </div>
    </div>
  </>
}


function SideBarSmScreen({ userName }: { userName: string }) {
  return (<>
    <div className="drawer">
      <input id="my-drawer" type="checkbox" className="drawer-toggle" />
      <div className="drawer-side">
        <label htmlFor="my-drawer" className="drawer-overlay"></label>
        <SideBarComponent userName={userName} />
        <label htmlFor="my-drawer" className="btn btn-primary drawer-button">Open drawer</label>
      </div>
    </div>
  </>)
}

function EditFeaturedItemModal() {
  const { data: products } = useQuery(['getUnFeaturedItems'], getUnFeaturedItems)
  return (<>
    <label htmlFor="my_modal_6" className="btn w-full bg-gray-800 border-none text-white btn-outline">Add Featured Item</label>
    <input type="checkbox" id="my_modal_6" className="modal-toggle" />
    <div className="modal">
      <div className="modal-box">
        <h3 className="font-bold text-lg">Add Featured</h3>
        <div className="overflow-y-auto h-40">
          <ul className="menu w-full rounded-box">
            {products && products?.map((item) => (<AddItemToFeatured key={item.id} item={item} />))}
          </ul>
        </div>
        <div className="modal-action">
          <label htmlFor="my_modal_6" className="btn">Close!</label>
        </div>
      </div>
    </div>
  </>)
}

function AddItemToFeatured({ item }: { item: typeViewProductData }) {
  const queryClient = useQueryClient()
  const { mutate: addToFeature } = useMutation(['addFeaturedItem'], feature, {
    onSuccess: () => {
      queryClient.invalidateQueries(['getFeaturedItems'])
    },
    onError: () => {
      toast.error('Error Adding Item to Featured')
    },
  })
  return <>
    <li className="mx-auto m-2"><button
      onClick={() => addToFeature({ id: item.id })}
    >{item.name}</button></li>
  </>
}
