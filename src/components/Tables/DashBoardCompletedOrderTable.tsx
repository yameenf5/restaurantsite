import { ColumnDef, flexRender, getCoreRowModel, getPaginationRowModel, useReactTable } from "@tanstack/react-table"
import { typeOrderView } from "../../../types"
import { useMemo, useState } from "react"
import { GrFormNext, GrFormPrevious } from "react-icons/gr"

export function DashboardCompleteOrderTable({ dataArray, column }: { dataArray: typeOrderView[], column: ColumnDef<typeOrderView>[] }) {

  const [pageIndex, setPageIndex] = useState(0)
  const data = useMemo(() => dataArray, [dataArray]);
  const table = useReactTable({
    data: data,
    columns: column,
    getCoreRowModel: getCoreRowModel(),
    getPaginationRowModel: getPaginationRowModel(),
    state:{
      pagination:{
        pageSize: 3,
        pageIndex: pageIndex
      }
    }

  });
  return (<>
    <div>
    <div className="text-center mb-4 text-xl font-mono uppercase">Completed Orders</div>
      <div className="flex flex-col mb-5 mx-1 border border-black">
        <table className="min-w-full divide-y divide-gray-200">
          <thead>
            {table.getHeaderGroups().map(headerGroup => (
              <tr key={headerGroup.id}>
                {headerGroup.headers.map(header => (
                  <th key={header.id} className="px-6 py-3 text-xs font-bold text-left text-white uppercase border border-white bg-black" scope="col">
                    {header.isPlaceholder
                      ? null
                      : flexRender(
                        header.column.columnDef.header,
                        header.getContext()
                      )}
                  </th>
                ))}
              </tr>
            ))}
          </thead>
          <tbody>
            {table.getRowModel().rows.map((row) => (
              <tr key={row.id} className='border border-black" bg-white'>
                {row.getVisibleCells().map((cell) => (
                  <td className="whitespace-nowrap px-6 py-4 text-sm font-light text-black border border-slate-200 bg-gray-50" key={cell.id}>
                    {flexRender(cell.column.columnDef.cell, cell.getContext())}
                  </td>
                ))}
              </tr>
            ))}
          </tbody>
        </table>
      </div>
    </div>
    <div className="w-full text-center">
      <button className="btn  hover:bg-gray-700 text-white font-bold mx-1  -my-2 focus:outline-black focus:ring active-gray-300 rounded-none "
        onClick={() => setPageIndex(pageIndex - 1)}
      >
        <GrFormPrevious size={24} />
      </button>
      <button className="btn hover:bg-gray-700 text-white font-bold mx-1  -my-2 focus:outline-black focus:ring active-gray-300 rounded-none "
        onClick={() => setPageIndex(pageIndex + 1)}
      >
        <GrFormNext size={24} />
      </button>
    </div>
  </>)
}
