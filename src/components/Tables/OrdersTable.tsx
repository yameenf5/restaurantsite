import { ColumnDef, flexRender, getCoreRowModel, getFilteredRowModel, getPaginationRowModel, useReactTable } from "@tanstack/react-table"
import {  typeOrderView  } from "../../../types"
import { useMemo, useState } from "react"
import {GrFormNext, GrFormPrevious} from "react-icons/gr"

export function OrdersTable({ dataArray, column }: { dataArray: typeOrderView[] , column: ColumnDef<typeOrderView>[]}) {

  const [filtering, setFiltering] = useState('');
  const data = useMemo(() => dataArray, [dataArray]);
  const table = useReactTable({
    data: data,
    columns: column,
    getCoreRowModel: getCoreRowModel(),
    getFilteredRowModel: getFilteredRowModel(),
    getPaginationRowModel: getPaginationRowModel(),
    state: {
      globalFilter: filtering
    },
    onGlobalFilterChange: setFiltering,

  });
  return (<>
    <div>
      <div className="w-full mt-8 mx-auto -mb-4">
        <div className="relative mx-auto w-1/4">
          <label htmlFor="Search" className="sr-only"> Search </label>

          <input
            type="text"
            id="Search"
            placeholder="Search for..."
            className="w-full rounded-md border-gray-200 py-2.5 pe-10 shadow-sm sm:text-sm"
            value={filtering}
            onChange={(e) => setFiltering(e.target.value)}
          />

          <span className="absolute inset-y-0 end-0 grid w-10 place-content-center">
            <button type="button" className="text-gray-600 hover:text-gray-700">
              <span className="sr-only">Search</span>

              <svg
                xmlns="http://www.w3.org/2000/svg"
                fill="none"
                viewBox="0 0 24 24"
                strokeWidth="1.5"
                stroke="currentColor"
                className="h-4 w-4"
              >
                <path
                  strokeLinecap="round"
                  strokeLinejoin="round"
                  d="M21 21l-5.197-5.197m0 0A7.5 7.5 0 105.196 5.196a7.5 7.5 0 0010.607 10.607z"
                />
              </svg>
            </button>
          </span>
        </div>
      </div>
      <div className="flex flex-col m-10 border border-black">
        <table className="min-w-full divide-y divide-gray-200">
          <thead>
            {table.getHeaderGroups().map(headerGroup => (
              <tr key={headerGroup.id}>
                {headerGroup.headers.map(header => (
                  <th key={header.id} className="px-6 py-3 text-xs font-bold text-left text-white uppercase border border-white bg-black" scope="col">
                    {header.isPlaceholder
                      ? null
                      : flexRender(
                        header.column.columnDef.header,
                        header.getContext()
                      )}
                  </th>
                ))}
              </tr>
            ))}
          </thead>
          <tbody>
            {table.getRowModel().rows.map((row) => (
              <tr key={row.id} className='border border-black" bg-white'>
                {row.getVisibleCells().map((cell) => (
                  <td className="whitespace-nowrap px-6 py-4 text-sm font-light text-black border border-slate-200 bg-gray-50" key={cell.id}>
                    {flexRender(cell.column.columnDef.cell, cell.getContext())}
                  </td>
                ))}
              </tr>
            ))}
          </tbody>
        </table>
      </div>
    </div>
    <div className="w-full text-center">
      <button className="btn  hover:bg-gray-700 text-white font-bold mx-1  -my-2 focus:outline-black focus:ring active-gray-300 rounded-none "
        onClick={() => table.previousPage()}
      >
        <GrFormPrevious size={24}/>
      </button>
      <button className="btn hover:bg-gray-700 text-white font-bold mx-1  -my-2 focus:outline-black focus:ring active-gray-300 rounded-none "
        onClick={() => table.nextPage()}
      >
        <GrFormNext size={24}/>
      </button>
    </div>
  </>)
}
