import { ColumnDef, flexRender, getCoreRowModel, useReactTable } from "@tanstack/react-table"
import {  typeViewProductData } from "../../../types"
import { useMemo, } from "react"

export function FeaturedTable({ dataArray, column }: { dataArray: typeViewProductData[] , column: ColumnDef<typeViewProductData>[]}) {

  const data = useMemo(() => dataArray, [dataArray]);
  const table = useReactTable({
    data: data,
    columns: column,
    getCoreRowModel: getCoreRowModel(),
  });

  return (<>
    <div>
      <div className="flex flex-col m-10 border border-black">
        <table className="min-w-full divide-y divide-gray-200">
          <thead>
            {table.getHeaderGroups().map(headerGroup => (
              <tr key={headerGroup.id}>
                {headerGroup.headers.map(header => (
                  <th key={header.id} className="px-6 py-3 text-xs font-bold text-left text-white uppercase border border-white bg-black" scope="col">
                    {header.isPlaceholder
                      ? null
                      : flexRender(
                        header.column.columnDef.header,
                        header.getContext()
                      )}
                  </th>
                ))}
              </tr>
            ))}
          </thead>
          <tbody>
            {table.getRowModel().rows.map((row) => (
              <tr key={row.id} className='border border-black" bg-white'>
                {row.getVisibleCells().map((cell) => (
                  <td className="whitespace-nowrap px-6 py-4 text-sm font-light text-black border border-slate-200 bg-gray-50" key={cell.id}>
                    {flexRender(cell.column.columnDef.cell, cell.getContext())}
                  </td>
                ))}
              </tr>
            ))}
          </tbody>
        </table>
      </div>
    </div>
  </>)
}

