import logo from '../../assets/react.svg'
import { FiLogIn, FiLogOut } from 'react-icons/fi';
import { Link } from "react-router-dom"
import { getSession, signOut } from '../../utils/authSession';
import { useMutation, useQuery, useQueryClient } from '@tanstack/react-query';
import { toast } from 'react-hot-toast';
import { IsAdmin } from '../../Routes/IsAdmin';
import { IslogedIn } from '../../Routes/IslogedIn';
import { ImAddressBook } from 'react-icons/im'
import { MdMenuBook } from 'react-icons/md';
import { BiHistory } from 'react-icons/bi'
import { GiHamburgerMenu } from 'react-icons/gi'

const Navbar = () => {
  const queryClient = useQueryClient();
  const { data: userSession } = useQuery(['getSession'], getSession)
  const { mutate: signOutFn } = useMutation(['signOut'], signOut, {
    onSuccess: () => queryClient.invalidateQueries(['getSession']),
    onError: () => toast.error("INTERNAL SERVER ERROR")
  })
  const userName = userSession?.user.user_metadata.firstName
  
  return <>
    <div className="h-9">
      <header className="bg-black fixed z-20 top-0 left-0 w-full shadow-md transition duration-500 " >
        <nav className="flex items-center lg:max-w-screen-xl mx-auto px-6 py-3 ">
          <Link className="flex flex-grow" to={'/'}>
            <img className="w-10 cursor-pointer rounded-full bg-white" src={logo} alt="logo" />
            <p className="text-white text-xl poppins hidden md:block lg:block ml-2">JUST EAT! </p>
          </Link>
          <div className="flex items-center justify-end space-x-4">
            {<IsAdmin fallback={<></>}><Link to={"/admin"} className="text-white">Admin</Link></IsAdmin>}
            <Link to={'/menu'}><button className="text-white hidden lg:block" ><div> Menu </div></button></Link>
            {
              <IslogedIn fallback={<></>}>
                <IsAdmin fallback={<Link to={'/addressdetails'} className='text-white hidden lg:block' >Addresses </Link>}></IsAdmin>
                <IsAdmin fallback={<Link to={'/orderhistory'} className='text-white hidden lg:block'> Orders </Link>}></IsAdmin>
                <FiLogOut className="cursor-pointer w-6 h-6 text-white" onClick={signOutFn} />
                <p className="text-white text-xl poppins hidden md:block lg:block">Welcome! {userName}</p>
              </IslogedIn>
            }
          </div>
          <div className="flex items-center justify-end space-x-6 mx-3">
            <IslogedIn fallback={<Link to={'/signIn'}><button className="bg-black px-6 py-2 text-white poppins border border-white focus:border-white focus:ring-4 transform transition duration-700 hover:scale-105 z-30" >Sign In</button></Link>}></IslogedIn>
          </div>
          <IsAdmin fallback={<div className='lg:hidden '><NavigationSmallSecreen /></div>}></IsAdmin>
        </nav>
      </header>
    </div>
  </>
}

function NavigationSmallSecreen() {
  const queryClient = useQueryClient();
  const { mutate: signOutFn } = useMutation(['signOut'], signOut, {
    onSuccess: () => {
      queryClient.invalidateQueries(['getSession']);
    },
    onError: () => toast.error("INTERNAL SERVER ERROR")
  })
  return <>
    <div className="drawer ">
      <input id="my-drawer" type="checkbox" className="drawer-toggle" />
      <div className="drawer-content">
        <label htmlFor="my-drawer" className="btn btn-outline border-black text-white bg-black rounded-none drawer-button"><GiHamburgerMenu size={24} /></label>
      </div>
      <div className="drawer-side">
        <label htmlFor="my-drawer" className="drawer-overlay"></label>
        <ul className="menu p-4 w-2/3 h-full bg-gray-700 opacity-80 text-base-content flex flex-col justify-between z-50 -ml-2 rounded-2xl">
        <li></li>
          <li>
            <Link to={'/menu'}><button className="text-white ml-1 mt-1 " ><div className='ml-20'><div className='ml-1'>MENU</div><MdMenuBook size={56} /></div></button></Link>
          </li>
          <li>
            <IslogedIn fallback={<></>}>
              <Link to={'/addressdetails'} className='text-white' ><div className='ml-20'><div className='ml-1'>Address</div><ImAddressBook size={56} /></div></Link>
            </IslogedIn>
          </li>
          <li>
            <IslogedIn fallback={<></>}> <Link to={'/orderhistory'} className='text-white'><div className='ml-20'><div className='ml-3'> Orders </div><BiHistory size={56} /></div></Link></IslogedIn>
          </li>
          <li className='flex flex-row'>
            <label htmlFor="my-drawer" className="w-15 mx-auto btn bg-black btn-outline text-white rounded-none pt-4 drawer-button  mr-2">Close</label>
            <IslogedIn
              fallback={<><Link to={'/signin'}><div className="w-10 mx-auto btn bg-black btn-outline text-white rounded-none drawer-button "><FiLogIn size={24} />
              </div></Link></>}>
              <div className="w-10 mx-auto btn bg-black btn-outline text-white rounded-none  drawer-button p-2"
                onClick={() => signOutFn()}><FiLogOut size={24} /></div>
            </IslogedIn>
          </li>
        </ul>
      </div>
    </div >
  </>
}

export default Navbar

