import { createClient } from '@supabase/supabase-js'
import { Database } from '../../types/supabase';
const supabaseUrl = 'https://cikpvmqinjztudiontet.supabase.co'
const supabaseKey = import.meta.env.VITE_SUPABASE_KEY

export const supabaseClient = createClient<Database>(supabaseUrl, supabaseKey);
