import { supabaseClient } from "./supabaseClient";

export async function getSession() {
  const { data, error } = await supabaseClient.auth.getSession()

  if (error) throw error;
  return data.session;
}

export async function signOut(){
  await supabaseClient.auth.signOut();
}
