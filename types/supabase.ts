export type Json =
  | string
  | number
  | boolean
  | null
  | { [key: string]: Json | undefined }
  | Json[]

export interface Database {
  public: {
    Tables: {
      Categories: {
        Row: {
          categoryName: string
          created_at: string
          id: string
        }
        Insert: {
          categoryName: string
          created_at?: string
          id?: string
        }
        Update: {
          categoryName?: string
          created_at?: string
          id?: string
        }
        Relationships: []
      }
      CustomerDetails: {
        Row: {
          addressName: string
          area: string
          city: string
          created_at: string
          customerId: string
          houseNo: number
          id: string
          isDeleted: boolean
          landmark: string
          pincode: number
        }
        Insert: {
          addressName: string
          area: string
          city: string
          created_at?: string
          customerId?: string
          houseNo: number
          id?: string
          isDeleted?: boolean
          landmark: string
          pincode: number
        }
        Update: {
          addressName?: string
          area?: string
          city?: string
          created_at?: string
          customerId?: string
          houseNo?: number
          id?: string
          isDeleted?: boolean
          landmark?: string
          pincode?: number
        }
        Relationships: [
          {
            foreignKeyName: "CustomerDetails_customerId_fkey"
            columns: ["customerId"]
            referencedRelation: "users"
            referencedColumns: ["id"]
          }
        ]
      }
      MenuProduct: {
        Row: {
          category: string
          created_at: string
          createdById: string
          description: string
          id: string
          imageUrl: string
          isAvailable: boolean
          isDeleted: boolean
          isfeatured: boolean
          isLargePictureFeatured: boolean
          name: string
          price: number
          size: string
        }
        Insert: {
          category: string
          created_at?: string
          createdById?: string
          description: string
          id?: string
          imageUrl?: string
          isAvailable: boolean
          isDeleted?: boolean
          isfeatured?: boolean
          isLargePictureFeatured?: boolean
          name: string
          price: number
          size: string
        }
        Update: {
          category?: string
          created_at?: string
          createdById?: string
          description?: string
          id?: string
          imageUrl?: string
          isAvailable?: boolean
          isDeleted?: boolean
          isfeatured?: boolean
          isLargePictureFeatured?: boolean
          name?: string
          price?: number
          size?: string
        }
        Relationships: [
          {
            foreignKeyName: "MenuProduct_createdById_fkey"
            columns: ["createdById"]
            referencedRelation: "users"
            referencedColumns: ["id"]
          }
        ]
      }
      Orders: {
        Row: {
          addons: string | null
          created_at: string
          customerAddressDetailsId: string | null
          customerId: string
          customerName: string
          id: string
          isCancled: boolean
          isConfirmed: boolean
          item: string
          itemId: string
          orderMode: string
          orderType: string
          paid: boolean
          price: number
          quantity: number
          size: string
        }
        Insert: {
          addons?: string | null
          created_at?: string
          customerAddressDetailsId?: string | null
          customerId?: string
          customerName: string
          id?: string
          isCancled?: boolean
          isConfirmed: boolean
          item: string
          itemId: string
          orderMode?: string
          orderType: string
          paid: boolean
          price: number
          quantity: number
          size: string
        }
        Update: {
          addons?: string | null
          created_at?: string
          customerAddressDetailsId?: string | null
          customerId?: string
          customerName?: string
          id?: string
          isCancled?: boolean
          isConfirmed?: boolean
          item?: string
          itemId?: string
          orderMode?: string
          orderType?: string
          paid?: boolean
          price?: number
          quantity?: number
          size?: string
        }
        Relationships: [
          {
            foreignKeyName: "Orders_customerAddressDetailsId_fkey"
            columns: ["customerAddressDetailsId"]
            referencedRelation: "CustomerDetails"
            referencedColumns: ["id"]
          },
          {
            foreignKeyName: "Orders_customerId_fkey"
            columns: ["customerId"]
            referencedRelation: "users"
            referencedColumns: ["id"]
          },
          {
            foreignKeyName: "Orders_itemId_fkey"
            columns: ["itemId"]
            referencedRelation: "MenuProduct"
            referencedColumns: ["id"]
          }
        ]
      }
    }
    Views: {
      [_ in never]: never
    }
    Functions: {
      [_ in never]: never
    }
    Enums: {
      [_ in never]: never
    }
    CompositeTypes: {
      [_ in never]: never
    }
  }
}
