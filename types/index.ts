import { Database } from "./supabase"

export type typeUser = {
  firstName: string,
  lastName: string,
  email: string,
  password: string,
  PasswordConfirmation: string,
}

export type typeUserSignIn = {
  email: string,
  password: string,
}
export type typeAddProduct = {
  name: string,
  category: string,
  price: number,
  description: string
  isAvailable: boolean
  size: string
  image: FileList
}

export type typeViewProductData = Database["public"]["Tables"]["MenuProduct"]["Row"]
export type typeMenuProductEdit = Database["public"]["Tables"]["MenuProduct"]["Update"]
export type typeMenuProductInsert = Database["public"]["Tables"]["MenuProduct"]["Insert"]
export type typeOrderInsert = Database["public"]["Tables"]["Orders"]["Insert"]
export type typeOrderEdit = Database["public"]["Tables"]["Orders"]["Update"]
export type typeOrderView = Database["public"]["Tables"]["Orders"]["Row"]
export type typeAddressDetailsView = Database["public"]["Tables"]["CustomerDetails"]["Row"]
export type typeAddressDetailsEdit = Database["public"]["Tables"]["CustomerDetails"]["Update"]
export type typeAddressDetailsInsert = Database["public"]["Tables"]["CustomerDetails"]["Insert"]
export type typeCategoriesView = Database["public"]["Tables"]["Categories"]["Row"]
export type typeCategoriesInsert = Database["public"]["Tables"]["Categories"]["Insert"]
export type typeOrderWithAddress = {
  addons: string | null
  created_at: string
  customerAddressDetailsId: string | null
  customerId: string
  customerName: string
  id: string
  isCancled: boolean
  isConfirmed: boolean
  item: string
  itemId: string
  orderMode: string
  orderType: string
  paid: boolean
  price: number
  quantity: number
  size: string
  CustomerDetails: {
    addressName: string
    area: string
    city: string
    created_at: string
    customerId: string
    houseNo: number
    id: string
    isDeleted: boolean
    landmark: string
    pincode: number
  } | null
} 
